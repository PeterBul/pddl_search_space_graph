import com.sun.istack.internal.NotNull;
import org.gerryai.planning.model.domain.*;
import org.gerryai.planning.model.logic.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;


import org.gerryai.planning.parser.error.ParseException;
import org.gerryai.planning.parser.pddl.PDDLParserService;

import com.google.common.base.Optional;



public class SearchSpaceGraph{
    private final Domain domain;
    private final String[] predicates;
    private final int numberOfStates;
    private Node[] nodes;
    private MyAction[] myActions;



    public SearchSpaceGraph(Domain domain){
        this.domain = domain;
        this.predicates = createPredicatesFromParser();
        this.numberOfStates = (int) Math.pow(2, predicates.length);
        this.nodes = new Node[numberOfStates];
        this.myActions = makeActions();
        makeStates();
    }

    /**
     * This method uses the gerryai PDDL domain object associated with the SearchSpaceGraph object and extracts the
     * predicate names.
     * @return This returns a array of strings containing the predicates of the domain.
     */

    private String[] createPredicatesFromParser(){
        Set<Predicate> predicates = domain.getPredicates().asSet();
        String[] predicatesArray = new String[predicates.size()];
        int i = 0;
        for (Predicate predicate : predicates){
            predicatesArray[i++] = predicate.getName();
        }
        return predicatesArray;
    }


    /**
     * This method uses the gerryai PDDL domain object associated with the SearchSpaceGraph object and extracts the
     * actions of the domain creating MyAction objects.
     * @return Returns an array of MyAction objects.
     */
    private MyAction[] makeActions(){

        Set<Action> actions = domain.getActions().asSet();
        MyAction[] myActions = new MyAction[actions.size()];
        int i = 0;
        for (Action action : actions){
            myActions[i] = makeMyAction(action.getName(), action.getPrecondition(), action.getEffect());
            i++;
        }
        return myActions;

    }

    /**
     * This method create MyAction objects from the parameters of a PDDL action.
     * @param name The name of the action.
     * @param precondition The precondition of the action. This is an instance of the gerryai Precondition class.
     * @param effect The effect of the action. This is an instance of the gerryai Effect class.
     * @return Returns an instance of MyAction class.
     */
    private MyAction makeMyAction(String name, Precondition precondition, Effect effect){
        Map<String, Boolean> preconditionMap = precondition.getPrecondition().isPresent() ?
                buildFormula(precondition.getPrecondition().get()) : new HashMap<String, Boolean>();
        Map<String, Boolean> effectMap = effect.getEffect().isPresent() ?
                buildFormula(effect.getEffect().get()) : new HashMap<String, Boolean>();
        return new MyAction(preconditionMap, effectMap, name);
    }

    /**
     * This is a recursive method that takes a gerrryai formula and turns it into a Map representation. It builds the
     * formula bottom up from predicates, that are the lowest abstraction needed.
     * @param formula An instance of the gerryai Formula class.
     * @param predicateValue Boolean predicate value given by a higher recursion, where the Formula instance was of
     *                       type 'Not'.
     * @param formulaBuilder The Map carried down into the recursion to build the formula in Map form.
     * @return On the bottom it adds predicates with their name as key and the value of the predicate to the Map. It
     *          returns this upward having the formula being a collection of positive or negative predicate terms.
     */
    private Map<String, Boolean> buildFormula(Formula formula, Boolean predicateValue, Map<String,
            Boolean> formulaBuilder) {
        if (formula instanceof Predicate) {
            formulaBuilder.put(((Predicate) formula).getName(), predicateValue);
        } else if (formula instanceof Not) {
            buildFormula(((Not) formula).getFormula(), false, formulaBuilder);
        } else if (formula instanceof And) {
            List<Formula> conjuncts = ((And) formula).asList();
            for (Formula conjunct : conjuncts){
                buildFormula(conjunct, true, formulaBuilder);
            }
        }
        return formulaBuilder;
    }

    /**
     * This is a init method for the more detailed recursive buildFormula method. It runs buildFormula with init
     * parameters predicateValue = true and and empty HashMap as the formula builder. The method creates a Map
     * representing the formula.
     * @param formula A formula being an instance of the gerryai Formula class.
     * @return A Map representing a formula either being a precondition or effect.
     */
    private Map<String, Boolean> buildFormula(Formula formula){
        return buildFormula(formula, true, new HashMap<String, Boolean>());
    }


    /**
     * A method that takes all the actions an the object and peforms them, thereby turning the actions into a graph.
     * @param myActions The array of the MyAction instances that are to be performed.
     */
    private void performActions(@NotNull MyAction[] myActions){

        for(MyAction myAction : myActions){
            System.out.println("MyAction " + myAction.name);
            // This is the char array of a state. A state is an interpretation of the predicates, so the array has
            // a length equal to the number of predicates. Each predicate can either be '0' or '1'. Fill with default 0.
            char[] state = new char[predicates.length];
            Arrays.fill(state, '0');
            // This is the lock with the length of the predicates, so as to be able to lock each predicate.
            int[] lock = new int[predicates.length];
            int nrOfPreconditions = myAction.preconditions.size();
            // The number of bits needed to cycle will be the number of predicates (because with a bit for each
            // precondition, we can cycle all the states) - the number of predicates in the precondition (since we
            // don't need bits to cycle for predicates showing up in the preconditions, since they are already set).
            int nrOfBits = predicates.length - nrOfPreconditions;
            // Cycle through the predicates and set the lock and the state (the boolean values of the predicates).
            for (int i = 0; i < predicates.length; i++){
                // Get the value of the predicate given the precondition Map of the action. Null if not present.
                Boolean precondition = myAction.preconditions.get(predicates[i]);
                // If the precondition is not null, the predicate is present int the precondition and have to be locked.
                // The state of the precondition is then '1'. If not it will stay '0'.
                if(precondition != null){
                    lock[i] = 1;
                    state[i] = precondition ? '1' : '0';
                }

            }
            // With all states prepared and all locks set so the method will skip present predicates, cycle the states.
            cycleStates(Integer.parseInt(new String(state), 2), lock, nrOfBits, myAction);

        }

    }

    /**
     * Method used to cycle states while respecting the lock. It uses the concept that each state can be represented as
     * a binary number with every bit being a predicate. The lock denotes which bits that are not to be changed,
     * because they are present in the preconditions. This method creates links between nodes if they are to be
     * connected.
     * @param state This is the state we start with, which is all '0'.
     * @param lock This is the lock denoting which predicates so not change.
     * @param nrOfBits The number of bits, denoting how many predicates that should be changed.
     * @param myAction An instance of the MyAction class which we are performing.
     */
    private void cycleStates(int state, int[] lock, int nrOfBits, MyAction myAction){
        int targetState;

        for (int i = 0; i < Math.pow(2, nrOfBits); i++){

            targetState = getTargetState(state, myAction);
            System.out.print(intToBinaryString(state) + " -> ");
            System.out.println(intToBinaryString(targetState));

            nodes[state].addChild(nodes[targetState], myAction);

            state = nextState(state, lock);
        }
    }

    /**
     * A method that creates a node for every state possible. It uses the number of states calculated from the number
     * of predicates.
     */
    private void makeStates(){
        for(int state = 0; state <  numberOfStates; state++){
            nodes[state] = new Node(state);
        }
    }


    /**
     * A method that takes a state and turn it into a new state, while respecting the lock. Used to cycle all the
     * possible states of combinations of predicates.
     * @param state A state of the search space.
     * @param lock An array denoting which predicates are not to be changed.
     * @return A number denoting the state in integer form.
     */
    private int nextState(int state, int[] lock){
        boolean overFlow = false;
        char[] stateCharArray = intToBinaryString(state).toCharArray();
        for(int i = stateCharArray.length - 1; i >= 0; i--){
            if(lock[i] == 1) {
                continue;
            }
            if (stateCharArray[i] == '0'){
                stateCharArray[i] = '1';
                break;
            } else {
                stateCharArray[i] = '0';
            }
            if(i == 0){
                overFlow = true;
            }
        }
        if(overFlow){
            return -1;
        } else {
            return Integer.parseInt(new String(stateCharArray), 2);
        }

    }

    /**
     * A method to take a state and an instance of the MyAction class and find the state that the action maps to.
     * @param state State in integer form representing the binary number representing the state.
     * @param myAction The action used to map between states.
     * @return An int representing the target state.
     */
    private int getTargetState(int state, MyAction myAction){
        char[] targetState = intToBinaryString(state).toCharArray();
        Map<String, Boolean> effects = myAction.effect;
        for(int i = 0; i < predicates.length; i++){
            String s = predicates[i];
            Boolean effect = effects.get(s);
            if(effect != null){
                targetState[i] = effect ? '1' : '0';
            }
        }

        return Integer.parseInt(new String(targetState), 2);

    }

    /**
     * Method that takes the graph and turns it into a JSONObject containing the graph for use in JavaScript.
     * @return JSONObject represented as a String.
     */
    private String makeJSONArrays(){

        // creating JSONObject
        JSONObject jsonObject = new JSONObject();

        // create JSONArray for nodes
        JSONArray jsonNodes  = new JSONArray();
        for(Node node:nodes){
            // create LinkedHashMap for each node
            Map n = new LinkedHashMap(3);

            n.put("id", node.state);
            n.put("name", stringBinaryToFormula(intToBinaryString(node.state)));
            n.put("size", 20);
            jsonNodes.add(n);
        }

        jsonObject.put("nodes", jsonNodes);
        // create JSONArray for links
        JSONArray jsonLinks = new JSONArray();
        for(MyAction myAction : myActions){
            for(Link link: myAction.getLinks()){
                // create LinkedHashMap for each link
                Map l = new LinkedHashMap(4);
                l.put("source", link.start.state);
                l.put("target", link.end.state);
                l.put("myAction", link.name);
                l.put("self", link.start.state == link.end.state ? 1:0);
                jsonLinks.add(l);
            }
        }
        jsonObject.put("links", jsonLinks);

        System.out.println(jsonObject.toJSONString());

        // writing JSON to file:"JSONExample.json" in cwd
        try {
            PrintWriter pw = new PrintWriter("web/JSONGraph.json");
            pw.write(jsonObject.toJSONString());

            pw.flush();
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return jsonObject.toJSONString();
    }

    /**
     * A method that prints the graph.
     * @param perAction This is a parameter denoting how to print the graph per action or per state. This is set when
     *                  calling the method. As the code is set now, I only use perAction=true.
     */
    private void printGraph(boolean perAction){
        if(perAction){
            for(MyAction myAction : myActions){
                System.out.println("MyAction: " + myAction.name);
                for(Link link : myAction.getLinks()){
                    System.out.println(intToBinaryString(link.start.state)
                            + " -> " + intToBinaryString(link.end.state));
                }
            }
            return;
        }
        for(int state = 0; state < numberOfStates; state++){
            Node stateNode = nodes[state];
            System.out.println("Source " + intToBinaryString(stateNode.state));
            for(Link link :stateNode.getChildren()){
                System.out.println(link.name + " -> " + intToBinaryString(link.end.state));
            }
        }
    }

    /**
     * Method to turn int into a String representing the int in binary form.
     * @param i The int to be transformed.
     * @return The String representing the integer in binary.
     */
    private String intToBinaryString(int i){
        return String.format("%" + predicates.length + "s", Integer.toBinaryString(i))
                .replace(' ', '0');
    }

    /**
     * Method that maps a binary number String into the formula it represents using the predicates of the domain.
     * @param name The binary String.
     * @return The formula as a String.
     */
    private String stringBinaryToFormula(String name){
        char[] charArray = name.toCharArray();
        StringBuilder stringBuilder = new StringBuilder();
        for( int i = 0; i < charArray.length; i++){
            String predicate = predicates[i];

            if(charArray[i] == '0'){
                stringBuilder.append("¬ ");
            }
            stringBuilder.append(predicate);
            if (i < charArray.length - 1){
                stringBuilder.append(", ");
            }
        }
        return stringBuilder.toString();
    }


    /**
     * Method to read a PDDL Domain file.
     * @param filepath filepath to the PDDL Domain file.
     * @return This returns a gerryai Domain instance.
     */
    public static Domain readPDDLDomain(String filepath){
        PDDLParserService parserService = new PDDLParserService();

        try {
            return parserService.parseDomain(new FileInputStream(filepath));

        } catch (ParseException ex) {
            throw new IllegalStateException("Could not parse PDDL file", ex);
        } catch (IOException ex) {
            throw new IllegalStateException("Could not read PDDL file for parsing", ex);
        }
    }

    public void printPDDL(){
        PDDLParserService parserService = new PDDLParserService();

        try {
            Domain domain = parserService.parseDomain(new FileInputStream("PDDLFiles/envelope/domain.pddl"));
            for(Action action : domain.getActions().asSet()){
                System.out.println(action.getName());
                Optional precondition = action.getPrecondition().getPrecondition();
                if(precondition.isPresent()){
                    System.out.println("Present");
                } else {
                    System.out.println("No preconditions");
                }

            }

        } catch (ParseException ex) {
            throw new IllegalStateException("Could not parse PDDL file", ex);
        } catch (IOException ex) {
            throw new IllegalStateException("Could not read PDDL file for parsing", ex);
        }
    }

    /**
     * Method setting all the settings and running all the methods needed to get the JSONGraph. This is the run method.
     * @return A string representing the JSON graph.
     */
    public static String getJSON(){
        String filepath = "PDDLFiles/envelope/domain.pddl";
        System.out.println("this works at least");
        SearchSpaceGraph searchSpaceGraph = new SearchSpaceGraph(readPDDLDomain(filepath));
        searchSpaceGraph.performActions(searchSpaceGraph.myActions);
        searchSpaceGraph.printGraph(true);
        return searchSpaceGraph.makeJSONArrays();
    }

    public static void main(String[] args) {
        getJSON();
    }

}
