/**
 * A class representing links between nodes in a graph. Each link has a name, a start node and an end node.
 *
 * @author Peter Cook Bulukin
 * @version 1.0
 * @since 2019-06-11
 */
public class Link {
    public String name;
    public Node start;
    public Node end;

    public Link(String name, Node start, Node end){
        this.name = name;
        this.start = start;
        this.end = end;
    }
}
