import java.util.ArrayList;
import java.util.List;

/**
 * A Node class used to make a graph. It adds links to MyAction objects at the same time.
 *
 * @author Peter Cook Bulukin
 * @version 1.0
 * @since 2019-06-11
 */
public class Node {
    public int state;
    private List<Link> children;

    public Node(int state){
        this.state = state;
        this.children = new ArrayList<Link>();
    }

    /**
     * Method used to add a child to the node.
     * @param childNode A node to be added as child to this node.
     * @param myAction An action to also add the link to the child.
     */
    public void addChild(Node childNode, MyAction myAction){
        Link link = new Link(myAction.name, this, childNode);
        children.add(link);
        myAction.addEdgeToAction(link);
    }

    /**
     * Getter method to get the children of this node.
     * @return A list of links to the children.
     */
    public List<Link> getChildren(){
        return children;
    }

}
