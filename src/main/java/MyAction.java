import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A class representing an action in PDDL. An action contains a precondition built by predicates and an effect built
 * by predicates.
 *
 * @author Peter Cook Bulukin
 * @version 1.0
 * @since 2019-06-11
 */
public class MyAction {

    Map<String, Boolean> preconditions;
    Map<String, Boolean> effect;
    String name;
    private List<Link> links;

    public MyAction(Map<String, Boolean> preconditions, Map<String, Boolean> effect, String name){
        this.preconditions = preconditions;
        this.effect = effect;
        this.name = name;
        this.links = new ArrayList<Link>();
    }

    /**
     * A method to add a link/edge to this instance.
     * @param link A link that is to be added to the MyAction abject.
     */
    public void addEdgeToAction(Link link){
        links.add(link);
    }

    /**
     * A getter method that gives the links of the action.
     * @return A list of links present int the MyAction instance.
     */
    public List<Link> getLinks(){
        return links;
    }





}
