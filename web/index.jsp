<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Network graph</title>
    <!-- Use CDN'd libraries: Jquery, d3js, BootStrap, google-code-prettify -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script src="//d3js.org/d3.v2.js"></script>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.1.1/css/bootstrap-combined.min.css" rel="stylesheet">
    <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/prettify/188.0.0/prettify.js"></script>
    <script src="FileSaver.js"></script>

</head>
<body>
<button class="btn btn-success" id="save_as_svg" value="">
    Save as SVG</button>
<script src="http://d3js.org/d3.v2.min.js?2.9.6"></script>
<form id="svgform" method="post" action="download.pl">
    <input type="hidden" id="output_format" name="output_format" value="svg">
    <input type="hidden" id="data" name="data" value="<svg xmlns=&quot;http://www.w3.org/2000/svg&quot; width=&quot;360&quot; height=&quot;180&quot;><circle class=&quot;little&quot; cx=&quot;174.83161041356965&quot; cy=&quot;51.01383503547295&quot; r=&quot;12&quot; fill=&quot;#644dd0&quot;/><circle class=&quot;little&quot; cx=&quot;205.98578789394733&quot; cy=&quot;99.81786096289974&quot; r=&quot;12&quot; fill=&quot;#28ec28&quot;/><circle class=&quot;little&quot; cx=&quot;325.877291408634&quot; cy=&quot;21.021927375230632&quot; r=&quot;12&quot; fill=&quot;#342fc5&quot;/></svg>">
</form>
</body>
</html>


<script>

    function create_d3js_graph(data) {

        var width = 960,
            height = 500;

        var color = d3.scale.category20c();

        var radius = d3.scale.sqrt()
            .range([0, 6]);

        var svg = d3.select("body").append("svg")
            .attr("width", width)
            .attr("height", height);

        var force = d3.layout.force()
            .size([width, height])
            .charge(-1000)
            .linkDistance(function (d) {
                return radius(d.source.size) + radius(d.target.size) + 200;
            });


        d3.json(data, function (graph) {
            force
                .nodes(graph.nodes)
                .links(graph.links)
                .on("tick", tick)
                .start();
            var link = svg.selectAll(".link")
                .data(graph.links)
                .enter().append("path")
                .attr("class", "link")
                .style("stroke", "#ccc")
                .style("fill", "none")
                .style("stroke-width", "2px")
                .attr('marker-end', function (d) {
                    if (d.self == 1) {
                        console.log(d.source.x + "," + d.source.y + " " + d.target.x + "," + d.target.y);
                        return 'url(#arrowheadSelf)';
                    } else {
                        return 'url(#arrowhead)';
                    }
                });

            var node = svg.selectAll(".node")
                .data(graph.nodes)
                .enter().append("g")
                .attr("class", "node")
                .call(force.drag);

            var nodelabels = svg.selectAll(".nodelabel")
                .data(graph.nodes)
                .enter()
                .append("text")
                .attr({
                    "x": function (d) {
                        return d.x;
                    },
                    "y": function (d) {
                        return d.y;
                    },
                    "class": "nodelabel",
                    "font-size": 12
                })
                .text(function (d) {
                    return d.name;
                });

            node.append("circle")
                .attr("r", function (d) {
                    return radius(d.size);
                })
                .style("fill", function (d) {
                    return "#FFF";
                })
                .style("stroke", "#000")
                .style("stroke-width", "1.5px");

            var edgepaths = svg.selectAll(".edgepath")
                .data(graph.links)
                .enter()
                .append('path')
                .attr({
                    'd': function (d) {
                        return linkPath(d, true)
                    },
                    'class': 'edgepath',
                    'fill-opacity': 0,
                    'stroke-opacity': 0,
                    'fill': 'blue',
                    'stroke': 'red',
                    'id': function (d, i) {
                        return 'edgepath' + i
                    }
                })
                .style("pointer-events", "none");

            var edgelabels = svg.selectAll(".edgelabel")
                .data(graph.links)
                .enter()
                .append('text')
                .style("pointer-events", "none")
                .attr({
                    'class': 'edgelabel',
                    'id': function (d, i) {
                        return 'edgelabel' + i
                    },
                    'dx': 80,
                    'dy': 0,
                    'font-size': 14,
                    'fill': '#000'
                });

            edgelabels.append('textPath')
                .attr('xlink:href', function (d, i) {
                    return '#edgepath' + i
                })
                .style("pointer-events", "none")
                .text(function (d, i) {
                    return d.myAction
                });


            // Arrowhead markers
            svg.append('defs').append('marker')
                .attr({
                    'id': 'arrowhead',
                    'viewBox': '0 0 10 10',
                    'refX': 18,
                    'refY': 2,
                    //'markerUnits':'strokeWidth',
                    'orient': 'auto',
                    'markerWidth': 10,
                    'markerHeight': 20,
                    'xoverflow': 'visible'
                })
                .append('svg:path')
                .attr('d', 'M0,0 V4 L4,2 Z')
                .attr('tranform', 'rotate(60)')
                .attr('fill', '#000')
                .attr('stroke', '#000');

            svg.append('defs').append('marker')
                .attr({
                    'id': 'arrowheadSelf',
                    'viewBox': '0 0 10 10',
                    'refX': 9.5,
                    'refY': 6,
                    //'markerUnits':'strokeWidth',
                    'orient': -13,
                    'markerWidth': 10,
                    'markerHeight': 20,
                    'xoverflow': 'visible'
                })
                .append('svg:path')
                .attr('d', 'M0,0 V4 L4,2 Z')
                .attr('fill', '#000')
                .attr('stroke', '#000');

            function tick() {
                link.attr("d", function (d) {
                    return linkPath(d)
                });


                node.attr("transform", function (d) {

                    var y = d.y - radius(d.size) / 2;

                    return "translate(" + d.x + "," + y + ")";
                });

                nodelabels.attr("x", function (d) {
                    return d.x - d.size;
                })
                    .attr("y", function (d) {
                        return d.y - d.size / 2;
                    });

                edgepaths.attr('d', function (d) {
                    return linkPath(d)
                });

                edgelabels.attr('transform', function (d, i) {
                    if (d.target.x < d.source.x) {
                        bbox = this.getBBox();
                        rx = bbox.x + bbox.width / 2;
                        ry = bbox.y + bbox.height / 2;
                        return 'rotate(180 ' + rx + ' ' + ry + ')';
                    }
                    else {
                        return 'rotate(0)';
                    }
                });
            }

            function linkPath(d, labelPath) {
                var x1 = d.source.x + 1,
                    y1 = d.source.y - 13,
                    x2 = d.target.x + 1,
                    y2 = d.target.y - 13,
                    dx = x2 - x1,
                    dy = y2 - y1,
                    ratio = Math.abs(dx / dy),
                    dr = Math.sqrt(dx * dx + dy * dy),
                    drawBackRatio = 10,

                    // Defaults for normal edge.
                    drx = dr,
                    dry = dr,
                    xRotation = 0, // degrees
                    largeArc = 0, // 1 or 0
                    sweep = 1; // 1 or 0

                if (d.self == 1 && labelPath) {
                    x1 = x1;
                    y1 = y1;
                    x2 = x1 + 20;
                    y2 = y1 + 90;
                    return "M" + x1 + " " + y1 + " L " + x2 + " " + y2;

                }

                // Self edge.
                if (x1 === x2 && y1 === y2) {
                    // Fiddle with this angle to get loop oriented.
                    xRotation = 30;

                    // Needs to be 1.
                    largeArc = 1;

                    // Change sweep to change orientation of loop.
                    sweep = 0;

                    // Make drx and dry different to get an ellipse
                    // instead of a circle.
                    drx = 20;
                    dry = 25;

                    // For whatever reason the arc collapses to a point if the beginning
                    // and ending points of the arc are the same, so kludge it.
                    x1 = x1 - 18;
                    y1 = y1 + 9;
                    x2 = x2 - 17;
                    y2 = y2 + 10;
                } else {
                    return "M" + x1 + " " + y1 + " L " + x2 + " " + y2;
                }

                return "M" + x1 + "," + y1 + "A" + drx + "," + dry + " " + xRotation + "," + largeArc + "," + sweep + " " + x2 + "," + y2;
            }
        });
    }

    function submit_download_form(output_format)
    {
        // Get the d3js SVG element
        var svg = document.getElementsByTagName("svg")[0];
        // Extract the data as SVG text string
        var svg_xml = (new XMLSerializer).serializeToString(svg);

        // Submit the <FORM> to the server.
        // The result will be an attachment file to download.
        var form = document.getElementById("svgform");

        form['output_format'].value = output_format;
        form['data'].value = svg_xml ;
        form.submit();

    }

    function download_svg(){
        // Get the d3js SVG element
        console.log("Button clicked");
        var svg = document.getElementsByTagName("svg")[0];
        // Extract the data as SVG text string
        var svg_xml = (new XMLSerializer).serializeToString(svg);

        var blob = new Blob([svg_xml], {type: "image/svg+xml"});
        saveAs(blob, "PDDLSearchSpace.svg")


    }

    $(document).ready(function() {
        create_d3js_graph("JSONGraph.json")
        $("#save_as_svg").click(function() { download_svg(); });
    })




</script>